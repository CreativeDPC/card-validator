import { useContext, useState } from 'react';
import { Card } from '../models/Card.model';
import { CardContext } from './CardContext';


export const useForm = (initialState = {}) => {

    const { card , setCard } = useContext(CardContext);
    const [values, setValues] = useState(initialState);


    const reset = () => {
        setValues(initialState);
        setCard(new Card());
    }


    const handleInputChange = ({ target }) => {
        const {value, maxLength, name} = target;

        const _setValue = () =>{
            setValues({
                ...values,
                [name]: value
            });
        }
        console.log(maxLength);
        if(maxLength != -1 && maxLength != undefined){
            if(value.length <= maxLength){
                _setValue();
            }
        }else{
            _setValue();
        }

        setCard({...card, [target.name]: target.value});

    }

    return [values, handleInputChange, reset];

}