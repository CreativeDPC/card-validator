import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import { ValidatorView } from '../views/ValidatorView';

export const AppRouter = () => {
    return (
        <Router>
            <Switch> 
                <Route exact path="/" component={ ValidatorView } />
                <Redirect to="/" /> 
            </Switch>
        </Router>                

    )
}