import { Card } from '../../components/Card'
import { FormValidator } from '../../components/FormValidator'
import styles from './ValidatorView.module.scss'

export const ValidatorView = ()=>{

    return (
        <div className={styles.validatorContainer} > 
            <div className={styles.formContainer}>
                <div className={styles.cardRegion}>
                    <Card />
                </div>
                <div className={styles.controlsRegion}>
                    <FormValidator />
                </div>
            </div>
        </div>
    )
}