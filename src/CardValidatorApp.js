import { useState } from "react";
import { CardContext } from "./hooks/CardContext";
import { Card } from "./models/Card.model";
import { AppRouter } from "./routers/AppRouter";
import './styles/styles.scss'

function CardValidatorApp() {
  
  const [ card, setCard ] = useState(new Card());

  return (
    <CardContext.Provider value={{ card, setCard }}>
        <AppRouter />
    </CardContext.Provider>    
  );
}

export default CardValidatorApp;
