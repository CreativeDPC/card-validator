import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const urlBase = process.env.REACT_APP_URL_CARD_SERVICE;

const newCard = async (card) =>{
    const MySwal = withReactContent(Swal);
    let _succes = false;

    const messageAlert = (text, type, footer = '') =>{
        MySwal.fire({
            icon: type,
            title: <p>{text}</p>,
            footer: footer,
        });
    }

    try {
        const resp = await  fetch(`${urlBase}/cards`, {
            method: 'POST',
            body: JSON.stringify(card),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if ( resp.ok ) {
            const cloudResp = await resp.json();
            const { statusCode, message } = cloudResp;
                if (statusCode == 200) {
                    _succes = true;
                    messageAlert(message, 'success', 'request processed correctly');
                } else {
                    messageAlert(message, 'error');
                }
        } else {
            messageAlert('Error', 'error');
        }

    } catch (err) {
        messageAlert(err, 'error');
    }

    return _succes;
}

export default {
    newCard
}