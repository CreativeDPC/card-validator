export class Card {
    cardNumber;
    cardName;
    cvv;
    expiration;

    constructor(data = {}){
        this.cardNumber = data.cardNumber || '';
        this.cardName = data.cardName || '';
        this.cvv = data.cvv || '';
        this.expiration = data.expiration || '';
    }
}
