import { MONTHS, YEARS } from '../../data/arrays';
import { useForm } from '../../hooks/useForm';
import styles from './FormValidator.module.scss'
import validators from '../Card/validators';
import { Card } from '../../models/Card.model';
import cardService from '../../services/Card.service';

export const FormValidator = () => {
    const [formValues, handleInputChange, reset] = useForm({
        cardNumber: "",
        cardName: "",
        month: "",
        year: "",
        cvv: ""
    });

    const { cardNumber, cardName, month, year, cvv } = formValues;
    const { isValidCard } = validators;
    const _isValidCard = isValidCard(cardNumber);

    const handleSubmitCard = async (e) => {
        e.preventDefault();
        const _isSucces = await cardService.newCard(new Card({
            cardNumber,
            cardName,
            cvv,
            expiration: `${month}/${year}`
        }));

        if(_isSucces) reset();
    }

    const isValid = () => {
        return (_isValidCard && cardNumber.length == 16) && cardNumber != '' && month != '' && year != '' && (cvv != '' && cvv.length == 3) ? true : false;
    }

    return (
        <form className={styles.formContainer} onSubmit={handleSubmitCard}>
            <span className={_isValidCard ? '' : styles.invalidCard}>Card Number</span>
            <input
                name="cardNumber"
                autoComplete="off"
                type="number"
                value={cardNumber}
                maxLength={16}
                onChange={handleInputChange}
            />
            <span>Card Name</span>
            <input
                name="cardName"
                autoComplete="off"
                type="text"
                value={cardName}
                onChange={handleInputChange} />
            <div className={styles.othersData}>
                <div>
                    <span>Expiration Date</span>
                    <select name="month" value={month} onChange={handleInputChange}>
                        {
                            MONTHS.map((m, i) => {
                                return <option key={i} value={m}>{m}</option>
                            })
                        }
                    </select>
                </div>
                <div>
                    <span></span>
                    <select name="year" value={year} onChange={handleInputChange}>
                        {
                            YEARS.map((y, i) => {
                                return <option key={i} value={y}>{y}</option>
                            })
                        }
                    </select>
                </div>
                <div>
                    <span>CVV</span>
                    <input
                        name="cvv"
                        autoComplete="off"
                        placeholder=""
                        type="number"
                        value={cvv}
                        maxLength={3}
                        onChange={handleInputChange} />
                </div>
            </div>
            {
                isValid() ?
                    <button type="submit" >Submit</button> :
                    <button type="submit" disabled>Submit</button>
            }

        </form>
    )
}