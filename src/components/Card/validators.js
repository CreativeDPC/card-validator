
import creditCardType from 'credit-card-type';
const formatCardNumber = (segment, cardNumber) => {
    switch (segment) {
        case 1:
            return cardNumber.toString().slice(0, 4).padEnd(4, '#')
        case 2:
            return cardNumber.toString().slice(4, 8).padEnd(4, '#')
        case 3:
            return cardNumber.toString().slice(8, 12).padEnd(4, '#')
        case 4:
            return cardNumber.toString().slice(12, 16).padEnd(4, '#')
        default:
            break;
    }
}

const typeCardLogo = (cardNumber) => {
    const number = cardNumber.toString().slice(0, 4)
    if (number.length > 3 ) {
        let visaCards = creditCardType(number);
        if (visaCards.length > 0) {
            const [card] = visaCards;
            switch (card.type) {
                case 'visa':
                    return <img src={`https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/visa.png`} />;
                case 'mastercard':
                    return <img src={`https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/MasterCard_logo.png/320px-MasterCard_logo.png`} />;
                default:
                    return <></>;
            }
        }
    }

    return <></>
}

const isValidCard = (cardNumber) => {
    let visaCards = creditCardType(cardNumber);
    return visaCards.length > 0 ? true :false;
}


export default {
    formatCardNumber,
    typeCardLogo,
    isValidCard
}