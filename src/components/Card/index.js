import { useContext } from 'react';
import { CardContext } from '../../hooks/CardContext';
import styles from './Card.module.scss'
import validators from './validators';

export const Card = () => {
    const { card:{cardNumber, cardName, month, year} } = useContext(CardContext);
    const {formatCardNumber, typeCardLogo} = validators;

    return (
        <div className={styles.cardContainer}>
            <div className={styles.card}>
                <div className={styles.header}>
                    <img src={`https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/chip.png`} />
                    {
                        typeCardLogo(cardNumber)
                    }
                </div>
                <div className={styles.body}>
                    <span>{formatCardNumber(1, cardNumber)}</span>
                    <span>{formatCardNumber(2, cardNumber)}</span>
                    <span>{formatCardNumber(3, cardNumber)}</span>
                    <span>{formatCardNumber(4, cardNumber)}</span>
                </div>
                <div className={styles.footer}>
                    <div >
                        <p>Card Holder</p>
                        <b>{cardName || 'Name'}</b>
                    </div>
                    <div >
                        <p>Expires</p>
                        <b>{month || 'MM'} / {year || 'YY'}</b>
                    </div>
                </div>
            </div>
        </div>
    )
}