import React from 'react';
import ReactDOM from 'react-dom';
import CardValidatorApp from './CardValidatorApp';

ReactDOM.render(
  <React.StrictMode>
    <CardValidatorApp />
  </React.StrictMode>,
  document.getElementById('root')
);
