
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

SPA (Single Page Application) que captura la informacion de una tarjeta y la valída.    


## Instalación

```bash
$ yarn install
```

## Ejecución

```bash
# Desarrollo
$ yarn run start


```

## Tecnologías Utilizadas

- [React Js](https://es.reactjs.org/) (Javascript)


## Prueba en vivo

[SPA Card Validator by Daniel Pérez](https://card-validator.web.app/)